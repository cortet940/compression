import binascii as b
import time

class HuffmanEncoder:
    def __init__(self, unencrypted_input):
        print("Unencrypted Message:", unencrypted_input)
        init_time = time.clock()
        first_list_node, last_list_node, list_size = self.count_frequency(unencrypted_input)
        tree = self.build_tree(first_list_node, last_list_node, list_size)
        self.encrypted_string = self.encode(tree, unencrypted_input)
        print("Encrypted Bits:", self.encrypted_string)
        print("Encrypted Text", b.b2a_base64(bytearray(self.encrypted_string, 'utf8')))
        end_time = time.clock() - init_time
        print("Time to process: ", end_time)


    def count_frequency(self, unencrypted_input):
        frequncy_list = []
        #List for letter and letter count
        first_list_node = None
        last_list_node = None
        list_size = 0
        #Remove all duplicates
        input_set = "".join(set(unencrypted_input))
        list_size = len(input_set)

        #For each character count when found in string
        for check_char in input_set:
            count = 0
            for input_char in unencrypted_input:
                if input_char == check_char:
                    count += 1

            frequncy_list.append((count, check_char))

        frequncy_list.sort()

        for element in frequncy_list:
            if not first_list_node:
                first_list_node = LinkedBinaryNode(element[1], element[0], False, None, None, None, None)
                last_list_node = first_list_node
            else:
                last_list_node = self.append_node(element[1], element[0], first_list_node, last_list_node)

        #Build linked list
        return first_list_node, last_list_node, list_size

    def append_node(self, data, value, first_list_node, last_list_node):
        current_node = first_list_node
        previous = None
        while current_node != None:
            previous = current_node
            current_node = current_node.next

        previous.next = LinkedBinaryNode(data, value, False, None, None, None, previous)
        last_list_node = previous.next


        return last_list_node


    def build_tree(self, first, last, size):
        first_list_node = current_node = first

        last_list_node = last
        list_size = size


        while list_size != 1:
            # Find first smallest
            first_to_swap = self.find_smallest_node(first_list_node)
            #swap with previous node
            temp_last_list_node = self.swap(first_to_swap, last_list_node)
            # Find second smallest
            second_to_swap = self.find_smallest_node(first_list_node)
            # Swap with second largest
            if not second_to_swap is last_list_node.previous:
                self.swap(second_to_swap, last_list_node.previous)
            last_list_node = temp_last_list_node

            #Build new nodes
            node_1_child = LinkedBinaryNode(last_list_node.data, last_list_node.value, True, last_list_node.left_child, last_list_node.right_child, None, None)
            node_2_child = LinkedBinaryNode(last_list_node.previous.data, last_list_node.previous.value, True, last_list_node.previous.left_child, last_list_node.previous.right_child, None, None)
            new_node_data = "".join([last_list_node.data, last_list_node.previous.data])
            new_node_value = int(last_list_node.value) + int(last_list_node.previous.value)

            last_list_node.previous.data = new_node_data
            last_list_node.previous.value = new_node_value
            last_list_node.previous.right_child = node_1_child
            last_list_node.previous.left_child = node_2_child
            last_list_node.previous.next = None
            last_list_node = last_list_node.previous
            list_size -= 1


        return first_list_node


    def find_smallest_node(self, first):
        current_node = first
        smallest = float("inf")
        smallest_node = None
        while not current_node is None:
            if current_node.value < smallest:
                smallest = current_node.value
                smallest_node = current_node
            current_node = current_node.next

        return smallest_node

    def swap(self, node_1, node_2):
        copy_of_1_value = node_1.value
        copy_of_1_data = node_1.data
        copy_of_1_left_child = node_1.left_child
        copy_of_1_right_child = node_1.right_child
        node_1.value = node_2.value
        node_1.data = node_2.data
        node_1.left_child = node_2.left_child
        node_1.right_child = node_2.right_child
        node_2.value = copy_of_1_value
        node_2.data = copy_of_1_data
        node_2.left_child = copy_of_1_left_child
        node_2.right_child = copy_of_1_right_child

        return node_2


    def encode(self, tree, unencrypted_input):
        encrypted_string = ""
        for character in unencrypted_input:
            encoded_string = ""
            current_node = tree
            while not current_node is None:
                if character == current_node.data:
                    encrypted_string = "".join([encrypted_string, encoded_string])
                    current_node = None
                # Check left child
                elif (not current_node.left_child is None) and (character in current_node.left_child.data):
                    current_node = current_node.left_child
                    encoded_string = "".join([encoded_string, '0'])
                # Check right child
                elif (not current_node.right_child is None) and (character in current_node.right_child.data):
                    current_node = current_node.right_child
                    encoded_string = "".join([encoded_string, '1'])
                else:
                    current_node = None

        return encrypted_string


class LinkedBinaryNode:
    def __init__(self, data, value, is_child, left_child, right_child, next, previous):
        self.data = data
        self.value = value
        self.is_child = is_child,
        self.left_child = left_child
        self.right_child = right_child
        self.next = next
        self.previous = previous





if __name__ == '__main__':
    HuffmanEncoder('Hello')